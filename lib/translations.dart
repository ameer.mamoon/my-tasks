import 'package:get/get.dart';

class Languages extends Translations{

  @override
  Map<String, Map<String, String>> get keys => {
    'en_us':{
      'Switch Theme':'Switch Theme',
      'Switch Language':'Switch Language',
      'About':'About',
      'Settings':'Settings',
      'Cancel':'Cancel',
      'Submit':'Submit',
      'Task name':'Task name'
    },
    'ar_sy':{
      'Switch Theme':'تغيّر النمط',
      'Switch Language':'تغيّر اللغة',
      'About':'معلومات عن التطبيق',
      'Settings':'الاعدادات',
      'Cancel':'إلغاء',
      'Submit':'تأكيد',
      'Task name':'اسم المهمة'
    },


  };

}