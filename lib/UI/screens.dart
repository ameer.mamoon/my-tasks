// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_tasks/STM/views.dart';

import '../main.dart';
import 'elements.dart';


class DefaultPage extends StatelessWidget {
  const DefaultPage({Key? key}) : super(key: key);



  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(

      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        leading: Builder(
            builder: (context) {
          return IconButton(
            icon: Icon(
              Icons.menu,
              color: MyApp.isLight ? Colors.black87 : null,
            ),
            onPressed: () {
              Scaffold.of(context).openDrawer();
            },
          );
        }),
      ),
      body: TasksList(),
      floatingActionButton: FloatingActionButton(
        onPressed: (){
          showModalBottomSheet(
              context: context,
              builder: (context){
                return InputWidget();
              }
              );

        },
        child: Icon(Icons.add),
      ),
      drawer:Drawer(
          child: ListView(
            children: [
              Container(
                width: double.infinity,
                height: size.height*0.25,
                padding: EdgeInsets.all(size.width*0.1),
                color: Theme.of(context).primaryColor,
                child: Row(
                  children: [
                    Expanded(
                      child: Container(
                        width: double.infinity,
                        height: double.infinity,
                        decoration:const BoxDecoration(
                            image: DecorationImage(
                              image: AssetImage('images/logo.png'),
                              fit: BoxFit.contain,
                            )
                        ),
                      ),
                    ),
                    SizedBox(
                      width: size.width*0.05,
                    ),
                    Expanded(
                      flex: 2,
                      child: FittedBox(child: const Text('My Tasks')),
                    )
                  ],
                ),
              ),
              Card(
                child: ListTile(
                  title: Text('Settings'.tr),
                  leading: Icon(Icons.settings),

                  onTap: () {
                      Get.toNamed('/settings');
                  },
                ),
              ),
              Card(
                child: ListTile(
                  title: Text('About'.tr),
                  leading: Icon(Icons.info_outline),
                  onTap: (){
                    showAboutDialog(
                        context: context,
                    applicationName: 'My Tasks',
                      applicationVersion: '1.0.0',
                    );
                    // showDialog(context: context, builder: (context){
                    //   return AlertDialog(content: Text('About'.tr),);
                    // });
                  },
                ),
              )
            ],
          )
      ),

    );
  }
}

class Settings extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        title: Text('Settings'.tr,style: TextStyle(
          color: MyApp.isLight ? Colors.black87 : Colors.white,
        ),),
        centerTitle: true,
        leading: IconButton(
          icon: Icon(Icons.arrow_back,color:  MyApp.isLight ? Colors.black87 : Colors.white,),
          onPressed: () => Get.back(),
        ),
      ),
      body: Center(
        child: Container(
          height: size.height*0.5,
          child: Column(
            children: [
              SelectButton(
                color: Theme.of(context).primaryColor,
                text: 'Switch Theme'.tr,
                onTap: (){
                  Get.changeThemeMode(MyApp.isLight ? ThemeMode.dark : ThemeMode.light);
                  MyApp.isLight = !MyApp.isLight;
                },
              ),
              SelectButton(
                color: Theme.of(context).primaryColor,
                text: 'Switch Language'.tr,
                onTap: (){
                  Get.updateLocale(MyApp.isEn?Locale('ar','sy'):Locale('en','us'));
                  MyApp.isEn = !MyApp.isEn;
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}

