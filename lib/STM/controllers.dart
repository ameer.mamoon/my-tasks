import 'package:get/get.dart';
import 'package:my_tasks/DB/DBhelper.dart';
import 'package:my_tasks/STM/models.dart';
import 'package:my_tasks/main.dart';



class TaskController extends GetxController{
  List<TaskModel> _models = [];

  List<TaskModel> get models => _models;

  TaskController._();

  static final TaskController controller = Get.put(TaskController._());

  factory TaskController() => controller ;


  @override
  void onInit() async {
    print('init');
    _models = await DBHelper().getTasks();
    update();
  }


  void add(TaskModel model) async {
    await DBHelper().insertTask(model);
    _models = await DBHelper().getTasks();
    await addNotification(model);
    update();
  }

  void remove(TaskModel model) async {
    await DBHelper().deleteTask(model.id);
    _models = await DBHelper().getTasks();
    print('del');
    await removeNotification(model.id);
    update();
  }


}