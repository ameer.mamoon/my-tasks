

class TaskModel{

  final int id;
  final String title;
  final DateTime time;

  TaskModel({this.id = -1,required this.title,required  this.time});

  factory TaskModel.map(Map<String,dynamic> m) =>
      TaskModel(
        id: m['id']! ,
        title: m['title'] ?? 'null',
        time: m['time'] != null ? DateTime.parse(m['time']):DateTime.now().add(Duration(minutes: 1)),
      );

  Map<String,dynamic> toMap() =>
      {
        "title":title,
        "time":time.toString()
      };

}