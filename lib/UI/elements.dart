import 'package:flutter/material.dart';
import 'package:my_tasks/STM/controllers.dart';
import 'package:my_tasks/STM/models.dart';
import 'package:get/get.dart';


class InputWidget extends StatelessWidget {

  static final TextEditingController textController = TextEditingController();
  static DateTime? _dateTime ;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      width: double.infinity,
      height: double.infinity,
      padding: EdgeInsets.all(size.width*0.035),
      child: LayoutBuilder(
        builder: (context,constrains){
          return Column(
            children:[
              Row(
                children: [
                  Expanded(
                      child: TextField(
                       controller: textController,
                        decoration: InputDecoration(
                          hintText: 'Task name'.tr
                        ),
                  ),
                  ),
                  IconButton(
                      onPressed:() async{
                        _dateTime = null;
                        DateTime Now = DateTime.now();
                        var selected = await showDatePicker(
                            context: context,
                            initialDate: Now,
                            firstDate: DateTime(Now.year,Now.month),
                            lastDate: DateTime(Now.year + 2 )
                        );
                      if(selected == null)
                        return ;

                     TimeOfDay timeNow = TimeOfDay.now();
                     var time = await showTimePicker(
                         context: context,
                         initialTime: timeNow
                     ) ;

                     if(time == null) return ;

                     _dateTime = DateTime(
                         selected.year,
                         selected.month,
                         selected.day,
                         time.hour,
                         time.minute
                     );


                      } ,
                      icon: Icon(Icons.date_range_outlined))
                ],
              ),
              Container(
                height: constrains.maxHeight*0.25,
                child: Row(
                  children: [
                    SelectButton(
                      text: 'Submit'.tr,
                      color: Theme.of(context).primaryColor,
                      onTap: (){
                        String title = textController.text;
                        DateTime? date = _dateTime;

                        textController.text = '';
                        _dateTime = null;

                        if(title.trim().isNotEmpty && date != null){
                          TaskController().add(
                            TaskModel(title: title, time: date),
                          );
                        }

                        Navigator.pop(context);
                      },
                    ),
                    SelectButton(
                      text: 'Cancel'.tr,
                      color: Colors.red,
                      onTap: (){
                        textController.text = '';
                        _dateTime = null;
                        Navigator.pop(context);
                      },
                    ),
                  ],
                ),
              ),
            ]
          );
        },
      ),
    );
  }
}


class SelectButton extends StatelessWidget {

  final Color color;
  final String text;
  final GestureTapCallback onTap;

  SelectButton({required this.color, required this.text,required  this.onTap});

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Expanded(
      child: GestureDetector(
        onTap: onTap,
        child: Container(
          width: double.infinity,
          height: double.infinity,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(size.width*0.025),
            color: color
          ),
          margin: EdgeInsets.all(size.width*0.025),
          padding: EdgeInsets.all(size.width*0.022),
          child: FittedBox(child: Text(text,style:const TextStyle(color: Colors.white),)),
        ),
      ),
    );
  }
}
