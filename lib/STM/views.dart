import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_tasks/STM/models.dart';

import 'controllers.dart';



class TasksList extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return GetBuilder<TaskController>(
        init: TaskController.controller,
        builder: (cont){
          return ListView.builder(
            itemCount: cont.models.length,
            itemBuilder: (context,i){
              return TaskWidget(cont.models[i]);
            },
          );
        }
    );
  }
}


class TaskWidget extends StatelessWidget {
  final TaskModel model;


  TaskWidget(this.model);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        title: Text('${model.title}'),
        subtitle: Text(model.time.toString()),
        leading: Icon(Icons.task),
        trailing: IconButton(
          icon: Icon(Icons.delete),
          onPressed: (){
            TaskController().remove(model);
          },
        ),
      ),
    );
  }
}
