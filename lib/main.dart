// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:my_tasks/STM/models.dart';
import 'package:my_tasks/UI/screens.dart';
import 'package:get/get.dart';
import 'package:my_tasks/translations.dart';



final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
FlutterLocalNotificationsPlugin();

Future selectNotification(String? payload) async {
  if (payload != null) {
    debugPrint('notification payload: $payload');
  }

}

Future<void> addNotification(TaskModel model)async{


  var androidPlatformChannelSpecifics = AndroidNotificationDetails(
    'alarm_notif',
    'alarm_notif',
    'Channel for Alarm notification',
    icon: 'icon',
    //sound: RawResourceAndroidNotificationSound('a_long_cold_sting'),
    largeIcon: DrawableResourceAndroidBitmap('icon'),
  );


  var platformChannelSpecifics = NotificationDetails(android: androidPlatformChannelSpecifics);

  await flutterLocalNotificationsPlugin.schedule(model.id, model.title, 'you have a task now',
      model.time, platformChannelSpecifics);


}

Future<void> removeNotification(int id) async{
  await flutterLocalNotificationsPlugin.cancel(id);
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
  FlutterLocalNotificationsPlugin();

  const AndroidInitializationSettings initializationSettingsAndroid =
  AndroidInitializationSettings('icon');


  final InitializationSettings initializationSettings = InitializationSettings(
    android: initializationSettingsAndroid,
  );
  await flutterLocalNotificationsPlugin.initialize(initializationSettings,
      onSelectNotification: selectNotification);

  runApp( MyApp());
}

class MyApp extends StatelessWidget {

  static bool isLight = true;
  static bool isEn = true;

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      theme: ThemeData.light().copyWith(
          primaryColor: Colors.blueGrey,
          floatingActionButtonTheme: FloatingActionButtonThemeData(
              backgroundColor: Colors.blueGrey
          )
      ),
      darkTheme: ThemeData.dark().copyWith(
          floatingActionButtonTheme: FloatingActionButtonThemeData(
              backgroundColor: Colors.teal
          )
      ),
      themeMode: ThemeMode.light,
      getPages: [
        GetPage(
            name: '/settings',
            page:() => Settings(),

        ),
      ],
      locale: Locale('en','us'),
      translations: Languages(),
      home: DefaultPage()
    );
  }
}
